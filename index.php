<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bewerber-Aufgabe</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.5.3/css/foundation-float.css" integrity="sha384-RgKKWex+zr/UMkk10qX6uL46Vvpo23H7QXDfntNEDvJ6mAkRLlIjjy4Pwv9Ebp0U" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.5.3/js/foundation.min.js" integrity="sha384-9ksAFjQjZnpqt6VtpjMjlp2S0qrGbcwF/rvrLUg2vciMhwc1UJJeAAOLuJ96w+Nj" crossorigin="anonymous"></script>
</head>
<body>
<?php
$host= "localhost";
$user = "root";
$pass = "";
$db = "mokom01";

$link = new mysqli ($host, $user, $pass, $db);
if (mysqli_connect_error()) {
  die('Could not connect');
}
$link->set_charset("utf8");

$vehicles_table = [];
if ($result = $link->query('SELECT id, category, displayed_name FROM vehicles')) {
  $categories = [];
  $vehicles_table = $result->fetch_all();
  foreach($vehicles_table as $row) {
    if (!array_key_exists($row[1], $categories)) {
      $categories[$row[1]] = [];
    }
    array_push($categories[$row[1]], $row[2]);
  }
}

if ($result = $link->query('SELECT id, first_name, last_name, age FROM registered_users')) {
  $users = $result->fetch_all();
  foreach($users as &$user) {
    if ($result = $link->query("SELECT vehicle FROM user_vehicles WHERE user = '$user[0]'")) {
      $vehicle_ids = $result->fetch_all();
      $vehicle_names = [];
      foreach($vehicle_ids as $vehicle_id) {
        foreach($vehicles_table as $vehicle) {
          if ($vehicle_id[0] == $vehicle[0]) {
            array_push($vehicle_names, $vehicle[2]);
          }
        }
      }
      $user[4] = implode(', ', $vehicle_names);
    }
  }
}

$_first_name = $_last_name = $_birthdate = $_vehicles = "";
$age = 0;
$_vehicles_boxes = [];
$first_name_err = $last_name_err = $birthdate_err = $vehicles_err = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST['first_name'])) {
    $first_name_err = "Vorname ist ein Pflichtfeld.";
  } else {
    $_first_name = $_POST['first_name'];
  }
  $_last_name = $_POST['last_name'];
  if (empty($_last_name)) {
    $last_name_err = "Nachname ist ein Pflichtfeld.";
  }
  $_birthdate = $_POST['birthdate'];
  if (empty($_birthdate)) {
    $birthdate_err = "Geburtsdatum ist ein Pflichtfeld.";
  }
  if (empty($_POST['vehicles']) || sizeof($_POST['vehicles']) < 1 || sizeof($_POST['vehicles']) > 5) {
    $vehicles_err = "Mindestens 1 Fahrzeug, höchstens 5.";
  } else {
    $_vehicles_boxes = $_POST['vehicles'];
  }

  if (strtotime($_birthdate)) {
    $age = date_diff(date_create($_birthdate), date_create('now'))->y;
  } else if (empty($birthdate_err)) {
    $birthdate_err = "Gültiges Datumsformat verwenden.";
  }
}
?>
    <div class="row">
        <div class="columns">
            <h1>Hello, world!</h1>
            <p>Bitte geben Sie hier Ihre Daten ein und wählen Sie Ihre Lieblingsfahrzeuge aus.</p>

            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">

                <!-- Vorname -->
                <div class="form-group">
                    <label for="form-field-first_name">Vorname:
                      <span <? echo !empty($first_name_err) ? 'style="color: red;"' : ''; ?>> * <?php echo $first_name_err;?></span>
                    </label>
                    <input type="text" name="first_name" id="form-field-first_name" value="<?php echo $_first_name;?>">
                </div>

                <!-- Nachname -->
                <div class="form-group">
                    <label for="form-field-last_name">Nachname:
                      <span <? echo !empty($last_name_err) ? 'style="color: red;"' : ''; ?>> * <?php echo $last_name_err;?></span>
                    </label>
                    <input type="text" name="last_name" id="form-field-last_name" value="<?php echo $_last_name;?>">
                </div>

                <!-- Geburtsdatum -->
                <div class="form-group">
                    <label for="form-field-birthdate">Geburtsdatum (Format TT.MM.YYYY):
                      <span <? echo !empty($birthdate_err) ? 'style="color: red;"' : ''; ?>> * <?php echo $birthdate_err;?></span>
                    </label>
                    <input type="text" name="birthdate" id="form-field-birthdate" value="<?php echo $_birthdate;?>">
                </div>
                <? $index = 1; ?>
                <? foreach ($categories as $category => $vehicles) : ?>
                  <div class="form-group">
                    <fieldset>
                      <legend><? echo $category;?></legend>
                      <? foreach ($vehicles as $vehicle) : ?>
                        <div class="form-check">
                          <input type="checkbox" name="vehicles[]" id="form-field-vehicles-<? echo $index;?>" value="<? echo $vehicle;?>" <? echo in_array($vehicle, $_vehicles_boxes) ? "checked" : "" ?>>
                          <label for="form-field-vehicles-<? echo $index;?>"><? echo $vehicle;?></label>
                        </div>
                        <? $index++;?>
                        <? endforeach;?>
                    </fieldset>
                  </div>
                <? endforeach;?>
                <p <? echo !empty($vehicles_err) ? 'style="color: red;"' : ''; ?>><?php echo $vehicles_err;?></p>
                <button type="submit" class="button">Absenden</button>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST" && $first_name_err == "" && $last_name_err == "" && $birthdate_err == 0 && $vehicles_err == "") {
  if ($link->query("INSERT INTO registered_users (first_name, last_name, age) values('$_first_name', '$_last_name', '$age')")) {
    $user_id = $link->insert_id;
    foreach($_vehicles_boxes as $vehicle_name) {
      foreach($vehicles_table as $vehicle) {
        if ($vehicle[2] == $vehicle_name) {
          $link->query("INSERT INTO user_vehicles (user, vehicle) values('$user_id', '$vehicle[0]')");
        }
      }
    }
    echo "Datensatz erstellt";
  } else {
    echo "Error: ".$link->error;
  }
}
?>
            </form>
        </div>
    </div>
    <div class="row">
      <div class="colums">
        <h3>Registrierte Benutzer</h3>
        <? if (!empty($users)) : ?>
          <table>
            <thead>
              <tr>
                <th>Vorname</th>
                <th>Nachname</th>
                <th>Alter</th>
                <th>Lieblingsfahrzeuge</th>
              </tr>
            </thead>
            <tbody>
              <? foreach ($users as $row) :  ?>
                <tr>
                  <td><? echo $row[1];?></td>
                  <td><? echo $row[2];?></td>
                  <td><? echo $row[3];?></td>
                  <td><? echo $row[4];?></td>
                </tr>
              <? endforeach;?>
            </tbody>
          </table>
        <? else : ?>
          <span>Keine registrierten Benutzer</span>
        <? endif; ?>
      </div>
    </div>
</body>
</html>
